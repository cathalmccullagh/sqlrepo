select * from bond
where CUSIP = '28717RH95'

select * from bond
order by maturity asc

select sum(quantity*price) as total_portfolio_value
from bond

select b.CUSIP, Year(b.maturity), sum(quantity*(coupon/100)) as annual_return from bond b
group by b.CUSIP, b.maturity

select * 
from bond b
join rating r on b.rating = r.rating
where r.ordinal <=3;


select rating, avg(price) as average_price, avg(coupon) as average_coupon 
from bond 
group by rating

select CUSIP, sum(coupon/price) as Yield, r.expected_yield as Expected_Yield
from bond 
join rating r on bond.rating = r.rating
group by CUSIP, r.expected_yield
having Sum(coupon/price) < r.expected_yield

