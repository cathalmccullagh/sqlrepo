
Select *
from trade t
join position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
where t.stock = 'mrk'
and p.trader_id = '1' 

Select SUM(t.size * t.price * (case when t.buy = 1 then -1 else 1 end)) as trader_DailyProfitLoss
from trade t
join position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
where t.stock = 'mrk' and  p.trader_id = '1'
and p.closing_trade_id != p.opening_trade_id and p.closing_trade_id is not null;


CREATE View Profit_and_Loss AS

Select tr.first_name, tr.last_name,
SUM(t.size * t.price * (case when t.buy = 1 then -1 else 1 end)) as 'trader_profit'
from trade t
join position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
join trader tr on p.trader_id = tr.id
where p.closing_trade_id is not null and p.closing_trade_id != p.opening_trade_id
group by tr.first_name, tr.last_name;